import groovy.json.JsonSlurperClassic
def buildConfig= [];

pipeline  {
	agent { label 'master' }
	options {
		preserveStashes()
	}
	parameters {
		booleanParam(name: 'beta', defaultValue: false, description: 'Bei einer Betafreigabe wird direkt in den Beta-Channel deployed. Willste Beta?')
		booleanParam(name: 'release', defaultValue: false, description: 'Bei der Releasefreigab geht die version direkt Release. Willste Release?')
		booleanParam(name: 'reallyRelease', defaultValue: false, description: 'Wirklich Release?')
	}
	environment {
		VERSION = sh(script: 'git describe --long --tags --match "v*" | sed "s/-/./g" | sed "s/v//" | sed -E "s/\\.g[0-9a-f]+//g"', , returnStdout: true).trim()
		ITERATIONVERSION = sh(script: 'git describe --long --tags --match "v*" | sed -E "s/v([^-]+)-.*/\\1/g"', , returnStdout: true).trim()
		PROJECT = sh(script: "echo $JOB_NAME | cut -d'/' -f2", returnStdout: true).trim()
		SOLUTION = sh(script: "echo net-core/${PROJECT}.sln", returnStdout: true).trim()
	}
	stages {
		stage('Version') {
			agent { label 'master' }
			steps {
				script {
					currentBuild.displayName  = sh(script: 'echo $VERSION', returnStdout: true).trim()
				}
				sh 'echo $PROJECT'
				sh 'echo $VERSION'
				sh 'echo $ITERATIONVERSION'
				sh 'echo $WORKSPACE'
				sh 'echo $SOLUTION'
				sh 'ls -lha'
				script {
					def configFile = new File("${WORKSPACE}/Jenkinsfile.json")
					buildConfig =  new JsonSlurperClassic().parseText(configFile.text);
					println buildConfig
				}
				
				stash allowEmpty: true, includes: 'thisdoesnotexist', name: 'zips_dotnet_8_0'
				stash allowEmpty: true, includes: 'thisdoesnotexist', name: 'nuget_dotnet_8_0'
				stash includes: 'net-core/NuGet.config', name: 'nugetconfig'
			} 
		}
			stage('dotnet_8_0') 
			{
				agent { label 'net_core_8_0_sonar' }
				options { skipDefaultCheckout() } 
				when { expression { buildConfig.any{ it.Build == 'dotnet_8_0'}  }}
				steps {
					checkout scm
					withSonarQubeEnv('antony') {
						sh label: "Start Sonar", script: "dotnet sonarscanner begin /k:\${PROJECT}_core80 /version:\$VERSION"
						script {
			
							withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: 'nexus', usernameVariable: 'NEXUS_PUSH_USER', passwordVariable: 'NEXUS_PUSH_PASSWORD']]) {
								sh label: "Restore Solution", script: "dotnet restore ${SOLUTION}"
								sh label: "Build Solution", script: "dotnet build --no-restore -c Release /p:Version=$VERSION ${SOLUTION}"
							}
							buildConfig.findAll{ it.Build == 'dotnet_8_0'}.each { entry ->
								script {
									withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: 'nexus', usernameVariable: 'NEXUS_PUSH_USER', passwordVariable: 'NEXUS_PUSH_PASSWORD']]) {
										def projectFile = "net-core/" + entry.Name + "/" + entry.Name + ".csproj"
										def projectName = entry.Name
							
										if (entry.MSI) {
											sh label: "Pulish winx64", script: """
												dotnet publish -c Release -p:Version=$VERSION ${projectFile} --self-contained true -r win-x64 -p:PublishDir=./publish/win
											"""
										}
										if ( entry.Docker ) {
											sh label: "Pulish docker build",  script: """
												dotnet publish --no-build -c Release -p:Version=$VERSION ${projectFile}  -p:PublishDir=./publish/docker
											"""
										}
										if ( entry.Nuget || entry.Nuget == [:] ) {
											sh label: "Build Nuget.pkg",  script: """
												dotnet pack --no-build -c Release -o=\$WORKSPACE/nuget -p:IncludeSymbols=true -p:Version=$VERSION ${projectFile}
											"""
										}
									}
								}
							}
						}
						sh  label: "Packing Publish Results to zip", script: '''
							mkdir output
							for projectFolder in net-core/*/publish/*; do
								[ ! -d "$projectFolder" ] && continue
					
								project=`echo $projectFolder | sed -r "s/net-core\\/([^\\]+)\\/publish\\/(.*)/\\1/g"`
								plattform=`echo $projectFolder | sed -r "s/net-core\\/([^\\]+)\\/publish\\/(.*)/\\2/g"`
								echo Project: $project / $plattform
								mv $projectFolder `basename $projectFolder`
								cd `basename $projectFolder`
								zip -1 -r -q ../output/${project}_${plattform}.zip * || true
								cd ..
								rm -rf `basename $projectFolder`
							done
						'''
						sh label: "Ending Sonarqube", script: "dotnet sonarscanner end || true"
						stash allowEmpty: true, includes: 'output/**/*', name: 'zips_dotnet_8_0'
						stash allowEmpty: true, includes: 'nuget/**/*', name: 'nuget_dotnet_8_0'
					}
				}
			} 

		
		stage("Wait for QualityGate") {
          steps { timeout(time: 1, unit: 'HOURS') { waitForQualityGate abortPipeline: true } }
        } 
		stage('Create deployable') {
			 parallel {
				stage('NuGet') {
					agent { label 'net_core_8_0_sonar' }
					when { expression { buildConfig.any{ it.Nuget || it.Nuget == [:] || it.MultiNuget || it.MultiNuget == [:] }  }}
					options { skipDefaultCheckout true }
					steps {
						script {
							unstash 'nuget_dotnet_8_0'
							unstash 'nugetconfig'
							sh 'mv net-core/NuGet.config NuGet.config'
							withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: 'nexus', usernameVariable: 'NEXUS_PUSH_USER', passwordVariable: 'NEXUS_PUSH_PASSWORD']]) {
								buildConfig.findAll{ it.Nuget || it.Nuget == [:]  || it.MultiNuget || it.MultiNuget == [:]}.each { entry ->
									def projectName = entry.Name
									sh label: "Publish $projectName", script: """
										dotnet nuget push "nuget/$projectName*.symbols.nupkg" -s groupware
									"""
								}
							}
						}
					} 
					post {
						always {
							mstest failOnError: false
						}
					}
				}
			
				stage('MSI') {
					agent { label 'msi' }
					when { expression { buildConfig.any{  it.MSI }  }}
					options { skipDefaultCheckout true }
					steps {
						cleanWs()
						unstash 'zips_dotnet_8_0'
						script {
							withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: 'nexus', usernameVariable: 'NEXUS_PUSH_USER', passwordVariable: 'NEXUS_PUSH_PASSWORD']]) {
								powershell label: "SetupBuildTools", script: """
									\$PWord = ConvertTo-SecureString -String "\$env:NEXUS_PUSH_PASSWORD" -AsPlainText -Force
									\$Credential = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList \$env:NEXUS_PUSH_USER, \$PWord
									Invoke-WebRequest -Uri "\${env:NEXUS_TESTENV_REPO}build_helper.zip" -OutFile build_helper.zip -Credential \$Credential
									Expand-Archive -LiteralPath build_helper.zip -DestinationPath .
								"""
								buildConfig.findAll{ it.MSI }.each { entry ->
									def projectName = entry.Name
									def msiUpgradeCode = entry.MSI.UpgradeCode
									def msiProductName = entry.MSI.ProductName ?: '""'
									def msiSortcut = entry.MSI.Shortcut ?: '""'
									def ServiceExe = entry.MSI.ServiceExe ?: '""'
									def isModule = (entry.MSI.IsModule) ? '$true' : '$false'
									def moduleName = (entry.MSI.ModuleName) ?: '""'
									def moduleAppName = (entry.MSI.ModuleAppName) ?: '""'
									def desktopShortcut = entry.MSI.DesktopShortcut ?: '""'
									def moduleExe = entry.MSI.ModuleExe ?: '""'
									def createWriteableAppConfig = (entry.MSI.CreateWriteableAppConfig) ? '$true' : '$false'
									
									powershell label: "Building $projectName", script:"""
										Write-Host $projectName
										Write-Host $msiUpgradeCode
										Write-Host $msiProductName
										Write-Host $msiSortcut
										Write-Host $ServiceExe
										Write-Host $moduleName
										Write-Host moduleAppName
										Write-Output "output/${projectName}_win.zip"
										Expand-Archive -LiteralPath output/${projectName}_win.zip -DestinationPath tmp
										Get-ChildItem -Recurse .\\tmp appsettings*Development.json | Remove-Item
										Get-ChildItem -Recurse .\\tmp appsettings.antonyHub.json | Remove-Item
										New-Item -ItemType directory -Path config -Force
										New-Item -ItemType directory -Path logs -Force
										New-Item -ItemType directory -Path data -Force
										Get-ChildItem .\\tmp\\. -Recurse
										.\\CreateMSI.ps1 -PublishPath .\\tmp -TmpPath .\\work -OutputPath .\\msis\\ -UpgradeCode $msiUpgradeCode -Version \${env:ITERATIONVERSION} -ProductName $msiProductName -ShortcutExe $msiSortcut -ServiceExe $ServiceExe -IsModule $isModule -DesktopShortcutExe $desktopShortcut -ModuleAppName $moduleAppName -ModuleName $moduleName -ModuleExe $moduleExe -ServiceExeName $msiProductName -CreateWriteableAppConfig $createWriteableAppConfig
										Remove-Item -Force -Recurse tmp  -ErrorAction SilentlyContinue
									"""
									withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: 'cert', usernameVariable: 'ANTONY_CERT_USERNAME', passwordVariable: 'ANTONY_CERT_PASSWORD']]) {
									bat label: "Sign $projectName", script: """
										echo Signtool Location: '%SignToolLocation%
										"%SignToolLocation%" sign /f %SIGN_KEY_FILE% /p %ANTONY_CERT_PASSWORD% /tr %SIGN_TIME_SERVER% msis/${msiProductName}_%ITERATIONVERSION%.msi
									"""
									}
								}
							}
						}
						stash includes: 'msis/*.msi', name: 'msis'
					}
				}
				stage ('Docker') {
					agent { label 'builddocker' }
					when { expression { buildConfig.any{  it.Docker }  }}
					options { skipDefaultCheckout true }
					steps {
						cleanWs()
						unstash 'zips_dotnet_8_0'
						script {
							withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: 'nexus', usernameVariable: 'NEXUS_PUSH_USER', passwordVariable: 'NEXUS_PUSH_PASSWORD']]) {
								sh 'docker login $NEXUS_DOCKER_REGISTRY_NAME --username $NEXUS_PUSH_USER --password $NEXUS_PUSH_PASSWORD'
								sh 'curl --user $NEXUS_PUSH_USER:$NEXUS_PUSH_PASSWORD $NEXUS_TESTENV_REPO/build_helper.zip > build_helper.zip'
								sh 'unzip build_helper.zip'
								sh 'ls -lha output'
								buildConfig.findAll{ it.Docker }.each { entry ->
									script {
										def projectName = entry.Name
										def build = entry.Build
										def projectStartup = entry.Docker.Startup
										def dockerTemplate = entry.Docker.Dockerfile ?: "Dockerfile_$build"
										
										sh label: "Build&Push Dockercontainer for ${projectName}", script: """
											# ... create tmp folder with its content 
											rm -rf tmp
											mkdir tmp
											unzip output/${projectName}_docker.zip -d tmp
											# ... creating Dockerfile.tmp and replac the $PROJECT within
											cp ${dockerTemplate} Dockerfile.tmp
											sed -i "s/%PNAME%/${projectStartup}/g" Dockerfile.tmp
											# .. and build ist
											docker build -f Dockerfile.tmp -t docker.die-groupware.de/`echo ${projectName} | tr '[:upper:]' '[:lower:]'`:$ITERATIONVERSION .
											docker push docker.die-groupware.de/`echo ${projectName} | tr '[:upper:]' '[:lower:]'`:$ITERATIONVERSION
											docker rmi docker.die-groupware.de/`echo ${projectName} | tr '[:upper:]' '[:lower:]'`:$ITERATIONVERSION
											rm Dockerfile.tmp
										"""
									}
								}
							}
						}
					}
					post {
						always {
							sh 'docker logout'
						}
					}
				}
			}
		}
		stage('Deploy MSI') {
			agent { label 'net_core_8_0_sonar' }
			when { expression { buildConfig.any{  it.MSI }  }}
			options { skipDefaultCheckout true }
			steps {
				unstash 'msis'
				sh 'ls -lha msis'
				withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: 'nexus', usernameVariable: 'NEXUS_PUSH_USER', passwordVariable: 'NEXUS_PUSH_PASSWORD']]) {
					script {
						buildConfig.findAll{ it.MSI }.each { entry ->
							def releaseType = 0
							if (params.beta) {
								releaseType = 1
							}
							if (params.release && params.reallyRelease) {
								releaseType = 2
							}
							def productName = entry.MSI.ProductName
							sh """
								curl -X PUT "${ANTONYHUB_HQ_URI}api/admin?PackageName=${productName}&Version=${VERSION}&ReleaseType=${releaseType}&InstallationType=msi&Commit=${GIT_COMMIT}&GitUrl=${GIT_URL}" \
											-H "accept: text/plain" \
											-H "ApiKey: $ANTONYHUB_HQ_APIKEY" \
											-H "Content-Type: multipart/form-data" \
											-F "thePackage=@msis/${productName}_${ITERATIONVERSION}.msi"
							"""
						}
					}
				}
			}
		}
	}
}